package com.hope.luo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 账单分类（商品分类）
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_bill_type")
public class BillTypeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品分类名称
     */
    private String name;

    /**
     * 商品分类编号
     */
    private String number;

    /**
     * 备注
     */
    private String note;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    private LocalDateTime modifyDate;


}

package com.hope.luo.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收入和支出订单表
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("p_pay_order")
public class PayOrderEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消费人
     */
    private String name;

    /**
     * 消费人编号
     */
    private String number;

    /**
     * 1:现金；2:支付宝；3:花呗；4:微信；5:银行卡；
     */
    private Integer payType;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品分类码
     */
    private String goodsCode;

    /**
     * 订单类型（1:收入；2:支出）
     */
    private Integer orderType;

    /**
     * 系统订单号（本系统生成）
     */
    private String orderNo;

    /**
     * 真实第三方消费单号
     */
    private String extNo;

    /**
     * 订单时间
     */
    private LocalDateTime orderTime;

    /**
     * 订单状态（0:等待中；1:支付成功；2:支付失败）
     */
    private Integer orderState;

    /**
     * 订单备注
     */
    private String orderNote;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 修改时间
     */
    private LocalDateTime modifyDate;


}

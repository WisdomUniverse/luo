package com.hope.luo.vm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;


@Builder
@Data
@AllArgsConstructor
public class UserVm {

    private String name;
    private String sex;
}

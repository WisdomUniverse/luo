package com.hope.luo.vm;

import lombok.Data;

@Data
public class LoginUser {

    private String userName;
    //角色
    private String role;
    //权限
    private String authority;
}

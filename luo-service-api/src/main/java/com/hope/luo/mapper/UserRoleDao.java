package com.hope.luo.mapper;

import com.hope.luo.entity.UserRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface UserRoleDao extends BaseMapper<UserRoleEntity> {

}

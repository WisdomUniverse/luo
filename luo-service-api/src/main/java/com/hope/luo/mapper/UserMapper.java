package com.hope.luo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hope.luo.entity.UserEntity;
import com.hope.luo.pojo.entity.User;
import com.hope.luo.vm.LoginUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper extends BaseMapper<UserEntity> {

    @Select("SELECT u.`name` as userName, r.`name` as role, a.`name` as authority FROM\n" +
            "`user` u \n" +
            "join user_role ur on u.id=ur.user_id \n" +
            "join role r on ur.role_id=r.id \n" +
            "join authority_role ar on r.id=ar.role_id\n" +
            "join authority a on ar.authority_id=a.id\n" +
            "where u.`name` = #{name};")
    List<LoginUser> getUserRoleAndAuth(String name);
}

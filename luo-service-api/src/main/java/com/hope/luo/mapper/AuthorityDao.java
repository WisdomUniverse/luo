package com.hope.luo.mapper;

import com.hope.luo.entity.AuthorityEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface AuthorityDao extends BaseMapper<AuthorityEntity> {

}

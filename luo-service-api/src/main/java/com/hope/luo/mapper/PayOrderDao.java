package com.hope.luo.mapper;

import com.hope.luo.entity.PayOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收入和支出订单表 Mapper 接口
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface PayOrderDao extends BaseMapper<PayOrderEntity> {

}

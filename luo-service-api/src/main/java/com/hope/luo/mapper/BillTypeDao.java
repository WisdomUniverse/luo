package com.hope.luo.mapper;

import com.hope.luo.entity.BillTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账单分类（商品分类） Mapper 接口
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface BillTypeDao extends BaseMapper<BillTypeEntity> {

}

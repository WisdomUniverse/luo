package com.hope.luo.mapper;

import com.hope.luo.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface RoleDao extends BaseMapper<RoleEntity> {

}

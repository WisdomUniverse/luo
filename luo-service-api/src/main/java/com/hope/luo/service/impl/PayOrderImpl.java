package com.hope.luo.service.impl;

import com.hope.luo.entity.PayOrderEntity;
import com.hope.luo.mapper.PayOrderDao;
import com.hope.luo.service.PayOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收入和支出订单表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class PayOrderImpl extends ServiceImpl<PayOrderDao, PayOrderEntity> implements PayOrderService {

}

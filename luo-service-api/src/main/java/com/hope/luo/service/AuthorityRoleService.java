package com.hope.luo.service;

import com.hope.luo.entity.AuthorityRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户-权限表 服务类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface AuthorityRoleService extends IService<AuthorityRoleEntity> {

}

package com.hope.luo.service.impl;

import com.hope.luo.entity.UserRoleEntity;
import com.hope.luo.mapper.UserRoleDao;
import com.hope.luo.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class UserRoleImpl extends ServiceImpl<UserRoleDao, UserRoleEntity> implements UserRoleService {

}

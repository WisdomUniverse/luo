package com.hope.luo.service;

import com.hope.luo.entity.AuthorityEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface AuthorityService extends IService<AuthorityEntity> {

}

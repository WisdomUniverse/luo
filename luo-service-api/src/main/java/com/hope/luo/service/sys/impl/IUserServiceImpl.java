package com.hope.luo.service.sys.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hope.luo.entity.UserEntity;
import com.hope.luo.mapper.UserMapper;
import com.hope.luo.pojo.entity.User;
import com.hope.luo.service.sys.IUserService;
import org.springframework.stereotype.Service;

@Service
public class IUserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements IUserService {

    @Override
    public UserEntity findUserByName(String name) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        UserEntity user = null;
        try{
            user = this.getOne(queryWrapper);
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }
}

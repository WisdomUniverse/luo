package com.hope.luo.service;

import com.hope.luo.entity.PayOrderEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收入和支出订单表 服务类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface PayOrderService extends IService<PayOrderEntity> {

}

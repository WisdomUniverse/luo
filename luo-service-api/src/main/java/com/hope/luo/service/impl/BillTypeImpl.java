package com.hope.luo.service.impl;

import com.hope.luo.entity.BillTypeEntity;
import com.hope.luo.mapper.BillTypeDao;
import com.hope.luo.service.BillTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账单分类（商品分类） 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class BillTypeImpl extends ServiceImpl<BillTypeDao, BillTypeEntity> implements BillTypeService {

}

package com.hope.luo.service.handle;

import com.hope.luo.entity.UserEntity;
import com.hope.luo.pojo.entity.User;

public interface CommonHandleService {

    /**
     * 查询用户
     * @param id 用户id
     * @return User
     */
    UserEntity findUserById(Long id);
}

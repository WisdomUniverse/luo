package com.hope.luo.service.impl;

import com.hope.luo.entity.UserEntity;
import com.hope.luo.mapper.UserDao;
import com.hope.luo.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class UserImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

}

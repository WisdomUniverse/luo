package com.hope.luo.service.impl;

import com.hope.luo.entity.AuthorityEntity;
import com.hope.luo.mapper.AuthorityDao;
import com.hope.luo.service.AuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class AuthorityImpl extends ServiceImpl<AuthorityDao, AuthorityEntity> implements AuthorityService {

}

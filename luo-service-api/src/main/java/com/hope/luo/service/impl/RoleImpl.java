package com.hope.luo.service.impl;

import com.hope.luo.entity.RoleEntity;
import com.hope.luo.mapper.RoleDao;
import com.hope.luo.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class RoleImpl extends ServiceImpl<RoleDao, RoleEntity> implements RoleService {

}

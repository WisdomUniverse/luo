package com.hope.luo.service.impl;

import com.hope.luo.entity.AuthorityRoleEntity;
import com.hope.luo.mapper.AuthorityRoleDao;
import com.hope.luo.service.AuthorityRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-权限表 服务实现类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
@Service
public class AuthorityRoleImpl extends ServiceImpl<AuthorityRoleDao, AuthorityRoleEntity> implements AuthorityRoleService {

}

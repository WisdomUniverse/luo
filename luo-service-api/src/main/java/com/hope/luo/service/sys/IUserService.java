package com.hope.luo.service.sys;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hope.luo.entity.UserEntity;

public interface IUserService extends IService<UserEntity> {
    public UserEntity findUserByName(String name);
}

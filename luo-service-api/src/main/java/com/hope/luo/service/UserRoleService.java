package com.hope.luo.service;

import com.hope.luo.entity.UserRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface UserRoleService extends IService<UserRoleEntity> {

}

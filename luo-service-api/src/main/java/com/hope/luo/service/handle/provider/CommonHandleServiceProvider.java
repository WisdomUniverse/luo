package com.hope.luo.service.handle.provider;

import com.hope.luo.entity.UserEntity;
import com.hope.luo.pojo.entity.User;
import com.hope.luo.service.handle.CommonHandleService;
import com.hope.luo.service.sys.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 公共业务处理类
 */
@Slf4j
@Service
public class CommonHandleServiceProvider implements CommonHandleService {

    @Autowired
    private IUserService iUserService;

    @Override
    public UserEntity findUserById(Long id) {
        log.info("findUserById: id", id);
        UserEntity user = iUserService.getById(id);
        return user;
    }
}

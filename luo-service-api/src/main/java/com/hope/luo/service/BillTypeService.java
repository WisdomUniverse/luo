package com.hope.luo.service;

import com.hope.luo.entity.BillTypeEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账单分类（商品分类） 服务类
 * </p>
 *
 * @author luoxx
 * @since 2020-07-17
 */
public interface BillTypeService extends IService<BillTypeEntity> {

}

package com.hope.exception.handle;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ControllerAdvice， 是Spring3.2提供的新注解,它是一个Controller增强器,可对controller中被
 * @RequestMapping 注解的方法加一些逻辑处理。最常用的就是异常处理
 */
@ControllerAdvice
public class NoPermissionException {
    // 授权失败，就是说没有该权限  直接跳转到指定的页面  unAuth.html
    @ExceptionHandler(UnauthorizedException.class)
    public String handleShiroException(Exception ex) {
        return "unAuth";
    }

    @ResponseBody
    @ExceptionHandler(AuthorizationException.class)
    public String AuthorizationException(Exception ex) {
        return "权限认证失败";
    }
}

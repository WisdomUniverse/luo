package com.hope.exception;

import com.hope.exception.base.ErrorInterface;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserServiceError implements ErrorInterface {
    USER_NOT_FOUNT("10001", "用户信息未找到");

    String errorCode;
    String errorMsg;
}

package com.hope.exception;

import com.hope.exception.base.ErrorInterface;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Rest返回结果
 * @author Hucx
 */
@Data
public class ApiRestResult<T> implements Serializable {
    private String status;
    private String errorCode;
    private String errorMessage;
    private String sign;
    private T resp;

    /**
     * 返回成功结果
     * @return
     */
    public static ApiRestResult buildSuccess() {
        return buildSuccess(null);
    }

    /**
     * 返回成功结果
     * @param <T>
     * @return
     */
    public static <T> ApiRestResult buildSuccess(T t) {
        ApiRestResult restResult = new ApiRestResult();
        restResult.setStatus(StatusCode.SUCCESS.status);
        restResult.setResp(t);
//        restResult.setSign(restResult.calSign());
        return restResult;
    }

    /**
     * 返回失败结果
     * @param errorCode
     * @param errorMessage
     * @return
     */
    public static ApiRestResult buildError(String errorCode, String errorMessage) {
        ApiRestResult restResult = new ApiRestResult();
        restResult.setStatus(StatusCode.FAIL.status);
        restResult.setErrorCode(errorCode);
        restResult.setErrorMessage(errorMessage);
//        restResult.setSign(restResult.calSign());
        return restResult;
    }


    /**
     * 返回失败结果
     * @param error
     * @return
     */
    public static ApiRestResult buildError(ErrorInterface error) {
        return buildError(error.getErrorCode(), error.getErrorMsg());
    }


    /**
     * 计算签名
     * @return
     */
    /*private String calSign() {
        String str = JSONObject.toJSONString(this);
        return M5Utils.signJsonData(str, Constants.API_KEY);
    }*/
}

/**
 * 返回状态
 */
@AllArgsConstructor
enum StatusCode{
    SUCCESS("0000", "成功"),
    FAIL("9999", "失败"),
    ;

    String status;
    String desc;
}

package com.hope.exception.base;

import com.hope.exception.ApiRestResult;
import com.hope.exception.BusinessException;

import java.io.Serializable;

public interface ErrorInterface extends Serializable {

    //获取错误代码
    String getErrorCode();
    //获取错误信息
    String getErrorMsg();

    //转化为错误结果
    default ApiRestResult toApiResult() {
        return ApiRestResult.buildError(this);
    }
    //抛出错误异常
    default RuntimeException toException() {
        throw new BusinessException(this);
    }
    //抛出错误异常（不打印日志）
    default RuntimeException toNoPrintException() {
        throw new BusinessException(this.getErrorMsg(), this.getErrorCode(), false);
    }
    default RuntimeException toException(boolean isPage) {
        throw new BusinessException(this, isPage);
    }
    default RuntimeException toException(String detailsErrorMsg) {
        throw new BusinessException(this, detailsErrorMsg);
    }

}

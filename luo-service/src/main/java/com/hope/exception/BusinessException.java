package com.hope.exception;

import com.hope.exception.base.ErrorInterface;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String errorMsg;
    private String errorCode;
    boolean isPage = false;

    public BusinessException(String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

    public BusinessException(String errorMsg, String errorCode, boolean isPage) {
        super(errorMsg);
        this.errorMsg = errorMsg;
        this.errorCode = errorCode;
        this.isPage = isPage;
    }

    public BusinessException(ErrorInterface error) {
        super(error.getErrorMsg());
        this.errorMsg = error.getErrorMsg();
        this.errorCode = error.getErrorCode();
    }

    public BusinessException(ErrorInterface error, boolean isPage){
        super(error.getErrorMsg());
        this.errorCode = error.getErrorCode();
        this.errorMsg = error.getErrorMsg();
        this.isPage = isPage;
    }

    public BusinessException(ErrorInterface error, String detailsErrorMsg){
        super(error.getErrorMsg() + "," + detailsErrorMsg);
        this.errorCode = error.getErrorCode();
        this.errorMsg = error.getErrorMsg() + "," + detailsErrorMsg;
    }
}

package com.hope.config.shiro;

import com.hope.luo.entity.UserEntity;
import com.hope.luo.mapper.UserMapper;
import com.hope.luo.pojo.entity.User;
import com.hope.luo.service.sys.IUserService;
import com.hope.luo.vm.LoginUser;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 自定义Realm用于查询用户的角色和权限信息并保存到权限管理器
 * 类AuthRealm完成根据用户名去数据库的查询,并且将用户信息放入 shiro 中,供第二个类调用
 */
public class AuthRealm extends AuthorizingRealm {
    @Autowired
    IUserService iUserService;
    @Autowired
    SqlSessionTemplate sqlSession;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取登录用户名
        User user = (User) principalCollection.getPrimaryPrincipal();
        //添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        //查询用户权限
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class); //注入UserMapper
        List<LoginUser> userRoleAndAuthList = userMapper.getUserRoleAndAuth(user.getName());
        for (LoginUser userAuth : userRoleAndAuthList) {
            //添加角色
            simpleAuthorizationInfo.addRole(userAuth.getRole());
            //添加权限
            simpleAuthorizationInfo.addStringPermission(userAuth.getAuthority());
        }
        return simpleAuthorizationInfo;
    }

    //认证.登录
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //加这一步的目的是在Post请求的时候会先进认证，然后在到请求
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        //获取用户信息
        String name = authenticationToken.getPrincipal().toString();
        //根据用户名去数据库查询用户信息
        UserEntity user = iUserService.findUserByName(name);
        if (user == null){
            //这里返回后会报出对应异常
            return null;
        }else {
            //这里验证authenticationToken和simpleAuthenticationInfo的信息
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(user, user.getPassword(), getName());
            return simpleAuthenticationInfo;
        }
    }
}

package com.hope.config.aop;

import com.hope.utils.JsonUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 参考文章：https://blog.csdn.net/lmb55/article/details/82470388/
 * 注意：在完成了引入AOP依赖包后，一般来说并不需要去做其他配置。
 * 使用过Spring注解配置方式的人会问是否需要在程序主类中增加@EnableAspectJAutoProxy来启用，实际并不需要。
 * 因为在AOP的默认配置属性中，spring.aop.auto属性默认是开启的，
 * 也就是说只要引入了AOP依赖后，默认已经增加了@EnableAspectJAutoProxy。
 *
 *
 * 定义切面类，实现web层的日志切面
 *
 * 要想把一个类变成切面类，需要两步，
 * ① 在类上使用 @Component 注解 把切面类加入到IOC容器中
 * ② 在类上使用 @Aspect 注解 使之成为切面类
 *
 *
 * com.hope.controller.aop包下的切面类通过 @Pointcut定义的切入点为com.example.aop包下的所有函数做切人，
 * 通过 @Before实现切入点的前置通知，通过 @AfterReturning记录请求返回的对象。
 */
@Aspect
@Component
public class WebLogAspect {

    //日志
    private final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);

    /**
     * 定义切入点，切入点为com.hope.controller.aop下的所有函数
     */
    @Pointcut("execution(public * com.hope.controller.aop..*.*(..))")
    public void webLog(){}

    /**
     * 前置通知@Before：在某连接点之前执行的通知，除非抛出一个异常，
     * 否则这个通知不能阻止连接点之前的执行流程。
     *
     * 前置通知：在连接点之前执行的通知
     * @param joinPoint
     * @throws Throwable
     */
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        /*ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        logger.info("URL : " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD : " + request.getMethod());
        logger.info("IP : " + request.getRemoteAddr());
        logger.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));*/


        /**
         * 注意：这里用到了JoinPoint和RequestContextHolder。
         * 1）、通过JoinPoint可以获得通知的签名信息，如目标方法名、目标方法参数信息等；
         * 2）、通过RequestContextHolder来获取请求信息，Session信息；
         */
        logger.info("前置通知");
        //获取目标方法的参数信息
        Object[] obj = joinPoint.getArgs();
        //AOP代理类的信息
        joinPoint.getThis();
        //代理的目标对象
        joinPoint.getTarget();
        //用的最多 通知的签名
        Signature signature = joinPoint.getSignature();
        //代理的是哪一个方法
        logger.info("代理的是哪一个方法==>"+signature.getName());
        //AOP代理类的名字
        logger.info("AOP代理类的名字==>"+signature.getDeclaringTypeName());
        //AOP代理类的类（class）信息
        signature.getDeclaringType();

        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        //如果要获取Session信息的话，可以这样写：
        //HttpSession session = (HttpSession) requestAttributes.resolveReference(RequestAttributes.REFERENCE_SESSION);
        //获取请求参数
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String,String> parameterMap = new HashMap();
        while (enumeration.hasMoreElements()){
            String parameter = enumeration.nextElement();
            parameterMap.put(parameter,request.getParameter(parameter));
        }
        String str = JsonUtils.objectToJson(parameterMap);
        if(obj.length > 0) {
            logger.info("请求的参数信息为："+str);
        }
    }

    @AfterReturning(returning = "ret",pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        logger.info("RESPONSE : " + ret);
    }


    /**
     * 后置最终通知（目标方法只要执行完了就会执行后置通知方法）
     * @param joinPoint
     */
    @After(value = "webLog()")
    public void doAfterAdvice(JoinPoint joinPoint){
        logger.info("后置最终通知执行了!!!!");
    }
}

package com.hope.utils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.util.JSONPObject;
import java.io.IOException;
import java.util.TimeZone;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

public class JsonMapper extends ObjectMapper {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LoggerFactory.getLogger(JsonMapper.class);

    public JsonMapper() {
        (new Jackson2ObjectMapperBuilder()).configure(this);
        this.setSerializationInclusion(Include.NON_NULL);
        this.configure(Feature.ALLOW_SINGLE_QUOTES, true);
        this.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        this.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        this.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>() {
            public void serialize(Object value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
                jgen.writeString("");
            }
        });
    }

    public String toJsonString(Object object) {
        try {
            return this.writeValueAsString(object);
        } catch (IOException var3) {
            logger.warn("write to json string error:" + object, var3);
            return null;
        }
    }

    public String toJsonpString(String functionName, Object object) {
        return this.toJsonString(new JSONPObject(functionName, object));
    }

    public <T> T fromJsonString(String jsonString, Class<T> clazz) {
        if (!StringUtils.isEmpty(jsonString) && !"<CLOB>".equals(jsonString)) {
            try {
                return this.readValue(jsonString, clazz);
            } catch (IOException var4) {
                logger.warn("parse json string error:" + jsonString, var4);
                return null;
            }
        } else {
            return null;
        }
    }

    public <T> T fromJsonString(String jsonString, JavaType javaType) {
        if (!StringUtils.isEmpty(jsonString) && !"<CLOB>".equals(jsonString)) {
            try {
                return this.readValue(jsonString, javaType);
            } catch (IOException var4) {
                logger.warn("parse json string error:" + jsonString, var4);
                return null;
            }
        } else {
            return null;
        }
    }

    public JavaType createCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return this.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    public <T> T update(String jsonString, T object) {
        try {
            return this.readerForUpdating(object).readValue(jsonString);
        } catch (JsonProcessingException var4) {
            logger.warn("update json string:" + jsonString + " to object:" + object + " error.", var4);
        } catch (IOException var5) {
            logger.warn("update json string:" + jsonString + " to object:" + object + " error.", var5);
        }

        return null;
    }

    public JsonMapper enableEnumUseToString() {
        this.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        this.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        return this;
    }

    public ObjectMapper getMapper() {
        return this;
    }

    public static JsonMapper getInstance() {
        return JsonMapper.JsonMapperHolder.INSTANCE;
    }

    public static String toJson(Object object) {
        return getInstance().toJsonString(object);
    }

    public static String toJsonp(String functionName, Object object) {
        return getInstance().toJsonpString(functionName, object);
    }

    public static <T> T fromJson(String jsonString, Class<?> clazz) {
        return (T) getInstance().fromJsonString(jsonString, clazz);
    }

    private static final class JsonMapperHolder {
        private static final JsonMapper INSTANCE = new JsonMapper();

        private JsonMapperHolder() {
        }
    }
}

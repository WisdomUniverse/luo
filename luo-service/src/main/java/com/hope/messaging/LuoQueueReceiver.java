//package com.hope.messaging;
//
//import com.alibaba.fastjson.JSONObject;
//import com.hope.config.Constants;
//import com.hope.utils.JsonMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.annotation.Queue;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
///**
// * 队列信息接收  queuesToDeclare = @Queue(Constants.LUO_QUEUE) 默认创建接收的队列名称
// */
//@Slf4j
//@Component
//@RabbitListener(queuesToDeclare = @Queue(Constants.LUO_QUEUE), containerFactory="rabbitListenerContainerFactory")
//public class LuoQueueReceiver {
//    @RabbitHandler
//    public void process(List<JSONObject> dataList) {
//        log.info("从队列中获取数据============================================" , dataList);
//
//        String  s  = JsonMapper.toJson(dataList);
//        List<JSONObject> getList = JSONObject.parseArray(s, JSONObject.class);
//
//        log.info("getList内容============================================", getList);
//    }
//}

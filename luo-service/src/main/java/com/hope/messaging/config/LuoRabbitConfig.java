/*
package com.hope.messaging.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

*/
/**
 * 推送队列配置
 *//*

@Configuration
public class LuoRabbitConfig {

    static final String directExchange = "luo-exchange";

    static final String queueName = "luo_queue";

    */
/**
     * 队列
     * @return
     *//*

    @Bean
    public Queue userStatisticsQueue(){
        return new Queue(queueName, true);
    }

    */
/**
     *  DirectExchange 交换机
     * @return
     *//*

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(directExchange);
    }


    //绑定  将队列和交换机绑定, 并设置用于匹配键：luo_routing
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(userStatisticsQueue()).to(exchange()).with("luo_routing");
    }

}
*/

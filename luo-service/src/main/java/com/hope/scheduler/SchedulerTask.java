/*
package com.hope.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

*/
/**
 * 定时任务 推送信息到队列 luo_queue
 *//*

@Component
@Slf4j
@Service
@RequiredArgsConstructor
public class SchedulerTask {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Scheduled(cron = "1 * * * * ?")//每1分钟执行一次
    public void luoQueueTask() {
        log.info("==========luoQueueTask 定时器开始执行==========" );

        List<Map> dataList = new ArrayList<>();

        Map jsonObject  = new HashMap();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        jsonObject.put("created_time", df.format(new Date()));
        jsonObject.put("name", "luo");
        dataList.add(jsonObject);

        log.info("==========luoQueueTask 推送数据大小=========="+dataList.size()+"条");
        log.info("==========luoQueueTask 推送数据内容=========="+dataList);

        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.convertAndSend("luo-exchange", "luo_routing", dataList);
    }

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        */
/* HOUR_OF_DAY 指示一天中小时 *//*

        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH");
        System.out.println("一个小时前的时间：" + df.format(calendar.getTime())+":00:00");
        System.out.println("当前的时间：" + df.format(new Date())+":00:00");

    }
}
*/

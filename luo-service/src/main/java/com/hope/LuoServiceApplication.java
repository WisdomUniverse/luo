package com.hope;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;


//@SpringBootApplication 来标注一个主程序类
//说明这是一个Spring Boot应用
@SpringBootApplication
@EnableScheduling //开启定时任务
@MapperScan(basePackages = {"com.hope.luo.mapper"})
public class LuoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuoServiceApplication.class, args);
    }

}

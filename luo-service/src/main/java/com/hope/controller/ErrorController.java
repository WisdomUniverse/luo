package com.hope.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 处理对应响应错误，跳转到对应错误页面
 */
@Controller()
public class ErrorController {

    @GetMapping("/error/404")
    public String error404(){
        return "error/404";
    }

    @GetMapping("/error/500")
    public String error500(){
        return "error/500";
    }

    @RequestMapping("/error/unAuth")
    public String unAuth(){
        return "unAuth";
    }

    @RequestMapping("/err")
    public String err(){
        return "unAuth";
    }
}

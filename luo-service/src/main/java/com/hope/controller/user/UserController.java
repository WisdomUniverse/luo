package com.hope.controller.user;

import com.hope.exception.UserServiceError;
import com.hope.luo.entity.UserEntity;
import com.hope.luo.service.handle.CommonHandleService;
import lombok.NonNull;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class UserController {

    @Autowired
    private CommonHandleService commonHandleService;

    /**
     * 测试集成 mybatis-plus
     * @param id
     * @return
     */
    @GetMapping("/luo/user/getUserById")
    @ResponseBody
    public UserEntity getUserById(@NonNull Long id){
        UserEntity user = commonHandleService.findUserById(id);
        if (user == null) UserServiceError.USER_NOT_FOUNT.toException();
        return user;
    }

    /**
     * 登录请求
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response){
        response.setHeader("root", request.getContextPath());
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        // 等于null说明用户没有登录，只是拦截所有请求到这里，那就直接让用户去登录页面，就不认证了。
        // 如果这里不处理，那个会返回用户名不存在，逻辑上不合理，用户还没登录怎么就用户名不存在？
        if(null == userName) {
            return "login";
        }

        // 1.获取Subject
        Subject subject = SecurityUtils.getSubject();
        // 2.封装用户数据
        UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
        // 3.执行登录方法
        try{
            subject.login(token);
            return "redirect:/index";
        } catch (UnknownAccountException e){
            // 这里是捕获自定义Realm的用户名不存在异常
            request.setAttribute("msg","用户名不存在！");
        } catch (IncorrectCredentialsException e){
            request.setAttribute("userName",userName);
            request.setAttribute("msg","密码错误！");
        } catch (AuthenticationException e) {
            // 这里是捕获自定义Realm的认证失败异常
            request.setAttribute("msg","认证失败！");
        }

        return "login";
    }

    /**
     * 登录成功后首页
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/index")
    public String index(HttpServletRequest request, HttpServletResponse response){
        return "index";
    }

    /**
     * 注销请求
     * @return
     */
    @RequestMapping("/logout")
    public String logout(){
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            subject.logout();
        }
        return "login";
    }

    /**
     * 个人中心，需认证可访问
     */
    @RequestMapping("/user/index")
    /*@RequiresPermissions(value = "user")*/
    public String add(HttpServletRequest request){
        UserEntity bean = (UserEntity) SecurityUtils.getSubject().getPrincipal();
        /*request.setAttribute("userName", bean);*/
        return "user";
    }


    /**
     * 会员中心，需认证且角色为vip可访问
     */
    @RequestMapping("/vip/index")
    @RequiresPermissions(value = "vip")
    public String update(){
        return "vipIndex";
    }
}
